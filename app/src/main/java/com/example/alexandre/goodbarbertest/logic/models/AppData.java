package com.example.alexandre.goodbarbertest.logic.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class AppData implements Serializable {

    @SerializedName("items")
    public ArrayList<Item> items;

}
