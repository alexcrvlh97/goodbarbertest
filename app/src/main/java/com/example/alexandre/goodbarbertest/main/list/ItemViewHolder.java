package com.example.alexandre.goodbarbertest.main.list;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.alexandre.goodbarbertest.R;
import com.example.alexandre.goodbarbertest.app.GoodBarberTestApp;
import com.example.alexandre.goodbarbertest.logic.models.Item;


//implements the layout of each individual item
public class ItemViewHolder extends RecyclerView.ViewHolder {
    private View itemView;
    private int position;

    public ItemViewHolder(View itemView, final RecyclerViewAdapter adapter) {
        super(itemView);

        this.itemView = itemView;

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.selectItem(position);
            }
        });
    }

    public void configureContent(Item item, int position) {
        this.position = position;
        TextView tvTitle = itemView.findViewById(R.id.tv_item_title);
        tvTitle.setText(item.getTitle());
        TextView tvCategory = itemView.findViewById(R.id.tv_item_category);
        tvCategory.setText(item.getType());

        ImageView imageView = itemView.findViewById(R.id.iv_item_preview);

        String path  = GoodBarberTestApp.getContext().getFilesDir().getAbsolutePath();

        Glide.with(GoodBarberTestApp.getContext())
                .asBitmap()
                .load(Uri.parse("file://"+path+"/"+item.getId()+"Thumbnail.png"))

                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.DATA)
                        .fitCenter())
                .into(imageView);

    }


}
