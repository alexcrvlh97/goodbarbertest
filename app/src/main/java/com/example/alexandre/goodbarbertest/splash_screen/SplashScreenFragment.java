package com.example.alexandre.goodbarbertest.splash_screen;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.alexandre.goodbarbertest.R;
import com.example.alexandre.goodbarbertest.base.BaseFragment;
import com.example.alexandre.goodbarbertest.logic.models.AppData;
import com.example.alexandre.goodbarbertest.logic.models.Item;
import com.example.alexandre.goodbarbertest.logic.utils.DataManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jdeferred2.DeferredManager;
import org.jdeferred2.DoneCallback;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DefaultDeferredManager;
import org.jdeferred2.multiple.MultipleResults;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



enum AlertType{
    NO_WIFI,
    NO_LOCAL_STORAGE,
    SOMETHING_WENT_WRONG

}

public class SplashScreenFragment extends BaseFragment {
    public static String TAG_INITIAL_LOADER_FRAGMENT = "InitialLoaderFragment";


    private AppData appData;
    private ExecutorService executorService = Executors.newCachedThreadPool();
    public Context context;
    DataManager dataManager;

    public TextView downloadDescription;
    ProgressBar pb_download;

    String dataFileName = "data.json";

    String dataUrl= "https://api.goodbarber.net/front/get_items/826899/14560013/?local=1";

    public SplashScreenFragment() {

    }

    public static SplashScreenFragment newInstance() {

        Bundle args = new Bundle();

        SplashScreenFragment fragment = new SplashScreenFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);



        return inflater.inflate(R.layout.activity_splash_screen, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        context= getContext();

        pb_download = getView().findViewById(R.id.pb_download);
        dataManager = new DataManager();
        downloadDescription = getView().findViewById(R.id.download_description);

        try {
            if(isInternetAvailable())
                prepareData();
            else
                showAlertDialogWithType(AlertType.NO_WIFI);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setDownloadDescription(String downloadDescription) {
        this.downloadDescription.setText(downloadDescription);
    }

    public boolean isInternetAvailable() throws IOException, InterruptedException {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void showAlertDialogWithType(final AlertType alertType){


        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }


        switch (alertType){

            case NO_WIFI:
                builder.setTitle("No Internet Connection")
                        .setMessage("Updates are unavailable on offline mode. Please verify your internet connection")

                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                dataManager.isFilePresent(context, dataFileName).done(new DoneCallback<Boolean>() {
                                    @Override
                                    public void onDone(Boolean result) {
                                        if(result)
                                            setupData();
                                        else
                                            showAlertDialogWithType(AlertType.NO_LOCAL_STORAGE);
                                    }
                                });
                            }
                        })

                        .setNegativeButton("Try again", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    if(isInternetAvailable())
                                        prepareData();
                                    else
                                        showAlertDialogWithType(AlertType.NO_WIFI);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }                            }
                        })
                        .setCancelable(false)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

                break;
            case NO_LOCAL_STORAGE:
                builder.setTitle("No Downloaded Content Available")
                        .setMessage("Downloads are unavailable on offline mode. Please verify your internet connection")

                        .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    if(isInternetAvailable())
                                        prepareData();
                                    else
                                        showAlertDialogWithType(AlertType.NO_WIFI);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                break;
            case SOMETHING_WENT_WRONG:
                builder.setTitle("Somethin Went Wrong")
                        .setMessage("Sorry about that")

                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                getActivity().finish();
                                System.exit(0);

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

                break;
        }

    }

    private void setProgressAnimate(int progress)
    {
        Log.e("Progress: ", progress + "% ");

        ObjectAnimator progressAnimator = ObjectAnimator.ofInt(pb_download, "progress", progress);
        progressAnimator.setDuration(500);
        progressAnimator.setInterpolator(new DecelerateInterpolator());
        progressAnimator.start();

    }

    // For each item downloads the main and author images
    public void downloadContent(final String json ){
        setDownloadDescription("Downloading resources...");

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        final AppData data = gson.fromJson(json, AppData.class);

        ArrayList<Promise> promiseArray = new ArrayList<>();

        DeferredManager dm = new DefaultDeferredManager();

        for ( int i = 0; i < data.items.size(); i++) {

            final int itemId = data.items.get(i).getId();
            final int finalI = i;

            DataManager.getImage getAuthorImg = new DataManager.getImage(context);
            getAuthorImg.executeOnExecutor(executorService, data.items.get(i).getAuthorAvatarUrl());
            Promise p1 = getAuthorImg.promise().done(new DoneCallback<Bitmap>() {

                @Override
                public void onDone(Bitmap result) {
                    if(result==null){
                        showAlertDialogWithType(AlertType.SOMETHING_WENT_WRONG);
                        return;

                    }
                    dataManager.saveImage(context, result, itemId+"Author").done(new DoneCallback<String>() {
                        @Override
                        public void onDone(String result) {
                            if(result==null){
                                showAlertDialogWithType(AlertType.SOMETHING_WENT_WRONG);
                                return;

                            }else{
                            }
                            Log.i("saveImage: ", "Path - " + result);

                        }
                    });
                }
            });

            DataManager.getImage getImg = new DataManager.getImage(context);
            getImg.executeOnExecutor(executorService, data.items.get(i).getLargeThumbnail());

            Promise p2 = getImg.promise().done(new DoneCallback<Bitmap>() {

                @Override
                public void onDone(Bitmap result) {
                    if(result==null){
                        showAlertDialogWithType(AlertType.SOMETHING_WENT_WRONG);
                        return;

                    }
                    dataManager.saveImage(context, result, itemId+"Thumbnail").done(new DoneCallback<String>() {
                        @Override
                        public void onDone(String result) {
                            if(result==null){
                                showAlertDialogWithType(AlertType.SOMETHING_WENT_WRONG);
                                return;

                            }

                            Log.i("saveImage: ", "Path - " + result);

                        }
                    });
                }
            });
            promiseArray.add(p1);
            promiseArray.add(p2);

            setProgressAnimate(10 + (80 / data.items.size() * (finalI +1)));


        }


        dm.when(promiseArray).done(new DoneCallback<MultipleResults>() {
            @Override
            public void onDone(MultipleResults result) {
                dataManager.createJSON(context, dataFileName, json).done(new DoneCallback<Boolean>() {
                    @Override
                    public void onDone(Boolean result) {
                        setupData();
                    }
                });
            }
        });


    }


    //Checks if datafile is exists, if not starts the download
    public void prepareData() {
        setProgressAnimate(5);
        setDownloadDescription("Checking resources...");

        dataManager.isFilePresent(context, dataFileName).done(new DoneCallback<Boolean>() {
            @Override
            public void onDone(Boolean result) {
                if (result) {
                    setProgressAnimate(70);
                    setupData();
                } else {

                    DataManager.JsonTask jsonTask = new DataManager.JsonTask(dataUrl);
                    jsonTask.executeOnExecutor(executorService);

                    System.out.println("Downloading Data file...");

                    jsonTask.promise().done(new DoneCallback<String>() {
                        @Override
                        public void onDone(String result) {
                            System.out.println("Data File Donwloaded!");
                            Log.e("JSON: ", result);
                            downloadContent(result);

                        }
                    });


                }
            }

        });


    }

// Json parse to the corresponding Object and starts the main activity
    private void setupData() {


        GsonBuilder gsonBuilder = new GsonBuilder();
        final Gson gson = gsonBuilder.create();


        dataManager.readJSON(context, dataFileName).done(new DoneCallback<String>() {
            @Override
            public void onDone(String result) {
                System.out.println("Parsing data...");
                appData = gson.fromJson(result, AppData.class);


                setProgressAnimate(90);
                setDownloadDescription("Download Completed...");

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        setProgressAnimate(100);
                        SplashScreenActivity activity = (SplashScreenActivity)getActivity();
                        activity.openMainActivity(appData);

                    }
                });


            }
        });

    }


}


