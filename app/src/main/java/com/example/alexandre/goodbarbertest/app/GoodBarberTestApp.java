package com.example.alexandre.goodbarbertest.app;

import android.app.Application;
import android.content.Context;
import android.support.v7.app.AppCompatDelegate;

public class GoodBarberTestApp extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        context = this;
    }

    public static Context getContext(){
        return  context;
    }
    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
