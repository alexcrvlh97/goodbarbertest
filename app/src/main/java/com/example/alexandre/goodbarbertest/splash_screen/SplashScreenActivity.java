package com.example.alexandre.goodbarbertest.splash_screen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.example.alexandre.goodbarbertest.R;
import com.example.alexandre.goodbarbertest.base.BaseActivity;
import com.example.alexandre.goodbarbertest.base.BaseFragment;
import com.example.alexandre.goodbarbertest.logic.models.AppData;
import com.example.alexandre.goodbarbertest.logic.models.Item;
import com.example.alexandre.goodbarbertest.main.MainActivity;

import java.util.ArrayList;

public class SplashScreenActivity extends BaseActivity {


    BaseFragment visibleFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.NoActionBar);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_splash_screen);


        openSplashScreenFragment();



    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        visibleFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    public void openMainActivity(final AppData appData) {



        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                intent.putExtra("itemsList", appData.items);
                SplashScreenActivity.this.startActivity(intent);
                SplashScreenActivity.this.finish();
            }
        }, 2 * 1000);

    }

    private void openSplashScreenFragment() {
        SplashScreenFragment fragment = (SplashScreenFragment) getSupportFragmentManager().findFragmentByTag(SplashScreenFragment.TAG_INITIAL_LOADER_FRAGMENT);

        if (fragment != null) {
            visibleFragment = fragment;
        } else {
            visibleFragment = SplashScreenFragment.newInstance();
        }


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, visibleFragment, SplashScreenFragment.TAG_INITIAL_LOADER_FRAGMENT)
                .commit();


    }




}
