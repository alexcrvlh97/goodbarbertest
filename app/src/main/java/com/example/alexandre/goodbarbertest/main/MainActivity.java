package com.example.alexandre.goodbarbertest.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.View;

import com.example.alexandre.goodbarbertest.R;
import com.example.alexandre.goodbarbertest.base.BaseActivity;
import com.example.alexandre.goodbarbertest.logic.models.Item;
import com.example.alexandre.goodbarbertest.main.details.ItemDetailsFragment;
import com.example.alexandre.goodbarbertest.main.list.ItemListFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements ICallbackListener {

    public ArrayList<Item> itemsList;

    ItemDetailsFragment itemDetailsFragment;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        itemsList = (ArrayList<Item>) getIntent().getSerializableExtra("itemsList");
        configureItemListFragment();


    }


    public void configureItemListFragment(){

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        ItemListFragment itemListFragment = new ItemListFragment();

        Bundle args = new Bundle();
        args.putSerializable(ItemListFragment.ITEM_LIST_EXTRA, itemsList);
        itemListFragment.setArguments(args);

        transaction.replace(R.id.ll_main_activity, itemListFragment, ItemListFragment.TAG);

        transaction.commit();


        itemListFragment.setCallbackListener(this);

    }

    @Override
    public void onItemClickedCallback(int position) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        itemDetailsFragment  = new ItemDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ItemDetailsFragment.ITEM_EXTRA, itemsList.get(position));

        itemDetailsFragment.setArguments(args);

        transaction.replace(R.id.ll_main_activity, itemDetailsFragment, ItemDetailsFragment.TAG);
        transaction.addToBackStack(ItemDetailsFragment.TAG);
        transaction.commit();

    }
}
