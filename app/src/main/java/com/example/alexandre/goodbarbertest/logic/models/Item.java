package com.example.alexandre.goodbarbertest.logic.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Item implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("type")
    private String type;

    @SerializedName("subtype")
    private String subtype;

    @SerializedName("author")
    private String author;

    @SerializedName("title")
    private String title;

    @SerializedName("date")
    private String date;

    @SerializedName("content")
    private String content;

    @SerializedName("authorAvatarUrl")
    private String authorAvatarUrl;

    @SerializedName("largeThumbnail")
    private String largeThumbnail;

    //-------------------------------- GET/SET ------------------------------------

    public int getId() {
        return id;
    }

    public String type(){
        return type;
    }

    public String getType() {
        return type;
    }

    public String getSubtype() {
        return subtype;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public String getContent() {
        return content;
    }

    public String getAuthorAvatarUrl() {
        return authorAvatarUrl;
    }

    public String getLargeThumbnail() {
        return largeThumbnail;
    }



}
