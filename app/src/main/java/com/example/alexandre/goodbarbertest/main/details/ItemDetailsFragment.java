package com.example.alexandre.goodbarbertest.main.details;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.alexandre.goodbarbertest.R;
import com.example.alexandre.goodbarbertest.app.GoodBarberTestApp;
import com.example.alexandre.goodbarbertest.base.BaseFragment;
import com.example.alexandre.goodbarbertest.logic.models.Item;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class ItemDetailsFragment extends BaseFragment {

    public static final String TAG = ItemDetailsFragment.class.getSimpleName();
    public static final String ITEM_EXTRA = "item_extra";




    private Item item;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_details, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            Item item = (Item) bundle.getSerializable(ITEM_EXTRA);
            this.item = item;
            setBackButtonAction();
            configureFragment();
        }
    }
    public void configureFragment(){
        setTitle();
        setImages();
        setDateAndAuthor();
        setWebView();
    }

    public void setWebView(){
        TextView tvTitle = (TextView) getView().findViewById(R.id.tv_details_content);
        tvTitle.setText(Html.fromHtml(item.getContent()).toString());


    }
    public void setImages(){
        ImageView authorView = getView().findViewById(R.id.iv_author);
        ImageView thumbnailView = getView().findViewById(R.id.iv_thumbnail);

        String path  = GoodBarberTestApp.getContext().getFilesDir().getAbsolutePath();

        Glide.with(GoodBarberTestApp.getContext())
                .asBitmap()
                .load(Uri.parse("file://"+path+"/"+item.getId()+"Thumbnail.png"))

                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.DATA)
                        .centerCrop())
                .into(thumbnailView);

        Glide.with(GoodBarberTestApp.getContext())
                .asBitmap()
                .load(Uri.parse("file://"+path+"/"+item.getId()+"Author.png"))
                .apply(RequestOptions.circleCropTransform())
                .into(authorView);

    }

    public void setTitle() {
        TextView tvTitle = (TextView) getView().findViewById(R.id.tv_details_title);
        tvTitle.setText(item.getTitle());
    }

    public void setDateAndAuthor() {
        TextView tvAuthor = (TextView) getView().findViewById(R.id.tv_author_iinfo);
        tvAuthor.setText("by "+ item.getAuthor());


        SimpleDateFormat spf = new SimpleDateFormat("YYYY-MM-DD");
        Date newDate = null;
        try {
            newDate = spf.parse(item.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("MMMM dd yyyy");
        String newDateString = spf.format(newDate);



        TextView tvDate = (TextView) getView().findViewById(R.id.tv_date);
        tvDate.setText("on "+newDateString);

    }

    private void setBackButtonAction() {
        ImageButton backButton = (ImageButton) getView().findViewById(R.id.bt_back_button);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

    }





}
