package com.example.alexandre.goodbarbertest.main.list;

import android.app.Application;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alexandre.goodbarbertest.R;
import com.example.alexandre.goodbarbertest.app.GoodBarberTestApp;
import com.example.alexandre.goodbarbertest.base.BaseFragment;
import com.example.alexandre.goodbarbertest.logic.models.Item;
import com.example.alexandre.goodbarbertest.main.ICallbackListener;

import java.util.ArrayList;

public class ItemListFragment extends BaseFragment implements ICallbackListener {

    public static final String TAG = ItemListFragment.class.getSimpleName();

    private RecyclerView recyclerView;
    private ArrayList<Item> items;
    public static final String ITEM_LIST_EXTRA = "item_list_extra";
    private ICallbackListener listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.rv_items_list);

        return view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            items = (ArrayList<Item>) bundle.getSerializable(ITEM_LIST_EXTRA);
            configureRecyclerView();

        }

    }


    private void configureRecyclerView() {
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(GoodBarberTestApp.getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(items, this);
        recyclerView.setAdapter(adapter);
    }

    public void setCallbackListener(ICallbackListener listener) {
        this.listener = listener;
    }


    @Override
    public void onItemClickedCallback(int position) {
        listener.onItemClickedCallback(position);
    }

}
