package com.example.alexandre.goodbarbertest.main.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alexandre.goodbarbertest.R;
import com.example.alexandre.goodbarbertest.logic.models.Item;
import com.example.alexandre.goodbarbertest.main.ICallbackListener;

import java.util.ArrayList;

//Creates all View Holders and connects them to the corresponding item/data
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {


    public ArrayList<Item> items;
    private ICallbackListener listener;

    public RecyclerViewAdapter(ArrayList<Item> items,ICallbackListener listener){
        this.items = items;
        this.listener = listener;

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        view = inflater.inflate(R.layout.layout_list_item, parent, false);
        viewHolder = new ItemViewHolder(view, this);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            ((ItemViewHolder) holder).configureContent(items.get(position),position);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
    public void selectItem(int position) {
        listener.onItemClickedCallback(position);
    }
}
