package com.example.alexandre.goodbarbertest.logic.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.jdeferred2.DeferredManager;
import org.jdeferred2.Promise;
import org.jdeferred2.android.DeferredAsyncTask;
import org.jdeferred2.impl.DefaultDeferredManager;
import org.jdeferred2.impl.DeferredObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



//Class that handles downloads and saves all data
public class DataManager extends AppCompatActivity {


    static DeferredObject DObj;

    private ExecutorService executorService = Executors.newCachedThreadPool();

    private DeferredManager dm = new DefaultDeferredManager(executorService);

    public Promise<Boolean, Throwable, ?> isFilePresent(Context context, String fileName) {
        DObj = new DeferredObject();
        String path = context.getFilesDir().getAbsolutePath() + "/" + fileName;

        File file = new File(path);
        return DObj.resolve(file.exists());
    }

    public  Promise<Boolean, Throwable, ?> createJSON(Context context, String fileName, String jsonString){
        DObj = new DeferredObject();
        try {
            FileOutputStream fos = context.openFileOutput(fileName,MODE_PRIVATE);

            if (jsonString != null) {
                fos.write(jsonString.getBytes());
            }
            fos.close();

            return DObj.resolve(true);
        } catch (FileNotFoundException fileNotFound) {
            return DObj.resolve(false);
        } catch (IOException ioException) {
            return DObj.resolve(false);
        }
    }

    public Promise<String, Throwable, ?> readJSON(Context context, String fileName) {
        DObj = new DeferredObject();
        try {
            FileInputStream fis = context.openFileInput(fileName);
            InputStreamReader isr = new InputStreamReader(fis, "utf8");
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return DObj.resolve(sb.toString());

        } catch (FileNotFoundException fileNotFound) {
            Log.i("FileNotFoundException: ","JSON - "+fileNotFound);
            return null;
        } catch (IOException ioException) {
            Log.i("IOException: ","JSON - "+ioException);
            return null;
        }
    }

    public Promise<String, Throwable, ?> saveImage(Context context, Bitmap bitmap, String imgName){
        DObj = new DeferredObject();

        FileOutputStream fileOutputStream;
        String filepath = "";

        try {
            fileOutputStream = context.openFileOutput(imgName+".png", Context.MODE_PRIVATE);

            filepath = context.getFilesDir().getAbsolutePath();

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            fileOutputStream.close();
            Log.i("SaveComplete: ", " - "+filepath+"/"+imgName+".png");
            return DObj.resolve(filepath);
        } catch (Exception e) {
            Log.i("SaveException: ", " - "+ e);
            e.printStackTrace();
            return DObj.resolve(null);
        }
    }

    public static class JsonTask extends DeferredAsyncTask<String, String, String> {

        String urls = null;

        public JsonTask(String url){
            this.urls = url;
        }


        @Override
        protected String doInBackgroundSafe(String... strings) throws Exception {
            if(urls == null){

                return null;
            }
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(urls);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");

                }
                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

    }

    public static class getImage extends DeferredAsyncTask<String, Void, Bitmap> {
        public getImage(Context context) {
        }

        @Override
        protected Bitmap doInBackgroundSafe(String... strings) {
            String url = strings[0];

            try{
                InputStream is = (InputStream) new URL(url).getContent();
                Bitmap bm = BitmapFactory.decodeStream(is);

                return bm;
            } catch (MalformedURLException e){
                Log.i("MalformedURLException: ","getImage - "+e);
                return null;
            }catch (IOException e){
                Log.i("IOException: ","getImage - "+e);
                return null;
            }catch (Exception e){
                Log.i("Exception: ","getImage - "+e);
                return null;
            }
        }
    }


}
